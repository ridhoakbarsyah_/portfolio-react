const logotext = "R I D H O - P A G E S";
const meta = {
  title: "Ridho Akbarsyah Ramadhan",
  description: "I’m Ridho Akbarsyah Ramadhan",
};

const introdata = {
  title: "I'm Ridho Akbarsyah Ramadhan",
  animated: {
    first: "I'm Frontend Developer",
    second: "Lives in Central Java, Indonesia",
  },
  description: "Currently working in Central Java, Indonesia",
  your_img_url: "https://images.unsplash.com/photo-1614741118887-7a4ee193a5fa?auto=format&fit=crop&q=80&w=1974&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
};

const dataabout = {
  title: "About my self",
  aboutme:
    "A motivated person to learn something new and face new challenges.Graduated with a Bachelor's degree in Computer, Department of Software Engineering, Telkom Purwokerto Institute of Technology with a fairly good GPA. Have good knowledge about web development, mobile development and UI/UX Designer. Has joined the Software Engineering Student Association organization and participated in competitions at institutional and national levels. Furthermore, I have also developed my leadership and communication skills through my experience in several skills.",
};

const worktimeline = [
  {
    jobtitle: "Frontend Developer",
    where: "PT. Tristar Surya Gemilang",
    date: "January 2023 - October 2023",
  },
  {
    jobtitle: "Frontend Developer Intern",
    where: "CV. Bahira Studio Purwokerto",
    date: "July 2023 - September 2023",
  },
];

const skills = [
  {
    name: "HTML",
    value: 90,
  },
  {
    name: "CSS",
    value: 85,
  },
  {
    name: "Javascript",
    value: 80,
  },
  {
    name: "React",
    value: 70,
  },
  {
    name: "Next",
    value: 70,
  },
  {
    name: "PostgreSQL",
    value: 70,
  },
  {
    name: "MySQL",
    value: 70,
  },
];

const dataportfolio = [
  {
    img: "https://ifoxsoft.com/wp-content/uploads/2022/09/Logo-Satpol-PP-Vector-IfoxSoft-1024x768.webp",
    description: "SISAPPRA SATPOL PP DKI JAKARTA",
    link: "https://sisappra-satpolpp.jakarta.go.id/v1/dki/sisappra/#/login",
  },
  {
    img: "https://s3-symbol-logo.tradingview.com/bank-pembangunan-daerah-jawa-barat-dan-banten-tbk--600.png",
    description: "BANK BJB",
    link: "#",
  },
];

const contactConfig = {
  YOUR_EMAIL: "ridhoakbarsyah15@gmail.com",
  YOUR_ADDRESS: "Central Java, Indonesia",
  description: "Feel free ask to me. Thanks❤️",
};

const socialprofils = {
  github: "https://github.com/ridhoakbarsyah",
  instagram: "https://instagram.com/ridhoakbarsyah_",
  linkedin: "https://www.linkedin.com/in/ridhoakbarsyah/",
};

export { meta, dataabout, dataportfolio, worktimeline, skills, introdata, contactConfig, socialprofils, logotext };
